package com.example.pokedex


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.bumptech.glide.Glide
import com.example.pokedex.Common.Common
import com.example.pokedex.Common.Common.findPokemonByNum
import com.example.pokedex.Model.Pokemon

private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class PokemonDetail : Fragment() {

    private lateinit var pokemon_img:ImageView
    private lateinit var pokemon_name:TextView
    private lateinit var pokemon_height:TextView
    private lateinit var pokemon_weight:TextView

    lateinit var recycler_type: RecyclerView
    lateinit var recycler_weakness: RecyclerView
    lateinit var recycler_prev_evolution: RecyclerView
    lateinit var recycler_netx_evolution: RecyclerView


    //Objeto comun a todas las instancias de la clase (campos estaticos)
    companion object{
        private var instance:PokemonDetail?=null
        fun getInstance():PokemonDetail{
            if(instance == null)
                instance = PokemonDetail()
            return instance!!
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val itemView = inflater.inflate(R.layout.fragment_pokemon_detail, container, false)


        //----------------------------------------------------//
        //AQUI REVIENTA ESA MIERDA
        /* lo del video 3 no he entendido algunas cosas. por ejemplo no entiendo muy bien que hace el men con este string "num". porque en common se creo findPokemonByNum
        * y compara que el campo num que viene del API sea equals a num string. pero no se en que momento el num String de este if se llene de datos. ese man no
        * hace nada adicional
        * el video es https://www.youtube.com/watch?v=41YjI6SPkSg&t=916s y lo que he te interesa de codigo del man estaria desde min 8:18 hasta minuto 15:00 los minutos anteriores es solo diseño
        * El problema debe estar en el condicional if de adelante que recibe pokemon: Pokemon? pero la verdad soy muy ñoño y no capto el error
        * de resto no veo nada de raro porque solo es la asignacion de las variables a los id en el fragment y la carga de los datos.*/
        val pokemon: Pokemon?
        if (arguments!!.getString("num") == null)
            pokemon = Common.pokemonList[arguments!!.getInt("position")]
        else
            pokemon = findPokemonByNum(arguments!!.getString("num"))


        pokemon_img = itemView.findViewById(R.id.pokemon_image) as ImageView
        pokemon_name = itemView.findViewById(R.id.name) as TextView
        pokemon_height = itemView.findViewById(R.id.heigth) as TextView
        pokemon_weight = itemView.findViewById(R.id.weigth) as TextView

        recycler_netx_evolution = itemView.findViewById(R.id.recycler_next_evolution) as RecyclerView
        recycler_netx_evolution.setHasFixedSize(true)
        recycler_netx_evolution.layoutManager = LinearLayoutManager(activity,LinearLayoutManager.HORIZONTAL,false)

        recycler_prev_evolution = itemView.findViewById(R.id.recycler_prev_evolution) as RecyclerView
        recycler_prev_evolution.setHasFixedSize(true)
        recycler_prev_evolution.layoutManager = LinearLayoutManager(activity,LinearLayoutManager.HORIZONTAL,false)

        recycler_type = itemView.findViewById(R.id.recycler_type) as RecyclerView
        recycler_type.setHasFixedSize(true)
        recycler_type.layoutManager = LinearLayoutManager(activity,LinearLayoutManager.HORIZONTAL,false)

        recycler_weakness = itemView.findViewById(R.id.recycler_Weakness) as RecyclerView
        recycler_weakness.setHasFixedSize(true)
        recycler_weakness.layoutManager = LinearLayoutManager(activity,LinearLayoutManager.HORIZONTAL,false)


        setDetailPokemon(pokemon )

        return itemView
    }

    private fun setDetailPokemon(pokemon: Pokemon?) {
                    //Load Data
            Glide.with(activity!!).load(pokemon!!.img).into(pokemon_img)
            pokemon_name.text = pokemon.name
            pokemon_height.text = "height: "+pokemon.height
            pokemon_weight.text = "weight: "+pokemon.weight
    }
}
